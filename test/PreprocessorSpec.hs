module PreprocessorSpec where

import Test.Hspec
import Text.RawString.QQ
import qualified Citronella.Preprocessor as PP

spec :: Spec
spec =
  describe "trigraph substitution" $ do
    it "example from spec" $
      PP.substituteTrigraphs "??=define arraycheck(a, b) a??(b??) ??!??! b??(a??)" 
        `shouldBe` "#define arraycheck(a, b) a[b] || b[a]"
    it "example from spec 2" $
      PP.substituteTrigraphs [r|printf("Eh???/n");|]
        `shouldBe` [r|printf("Eh?\n");|]
