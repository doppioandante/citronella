module PreprocessorLexerSpec where

import Test.Hspec
import Text.RawString.QQ
import qualified Data.Text as T
import Citronella.PreprocessorLexer

scannerToText :: Either String [PreToken] -> Either String T.Text
scannerToText = fmap (T.concat . (Prelude.map getTokenText))
  where
    getTokenText (PreToken _ _ text) = text

spec :: Spec
spec =
  describe "Preprocessor lexing" $ do
    it "Example 6.4p5" $
      scanner "1Ex" 
        `shouldBe` (Right [PreToken (AlexPn 0 1 1) PPNumber "1Ex"])
    it "Example 6.4p6" $
      scanner "x+++++y"
        `shouldBe` (Right [PreToken (AlexPn 0 1 1) Identifier "x",PreToken (AlexPn 1 1 2) Punctuator "++",PreToken (AlexPn 3 1 4) Punctuator "++",PreToken (AlexPn 5 1 6) Punctuator "+",PreToken (AlexPn 6 1 7) Identifier "y"])
    it "skip block comment" $
      (scannerToText $ scanner "this should/* be here */become a space")
         `shouldBe` (Right "this should become a space")
    it "no nested comments" $
      (scannerToText $ scanner "this should/* be here */ */become a space")
         `shouldBe` (Right "this should  */become a space")
    it "block comment position preservation" $ True -- TODO
    it "skip line comment" $
      (scannerToText $ scanner "code // line comment \nmore code")
        `shouldBe` (Right "code \nmore code")