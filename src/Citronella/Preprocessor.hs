module Citronella.Preprocessor (substituteTrigraphs)
where

-- See section 5.1.1.2 (Translation phases, 1 through 4)

import qualified Data.Text as T

-- 5.2.1.1, 5.1.1.2.1
substituteTrigraphs :: T.Text -> T.Text
substituteTrigraphs = substituteTrigraphs' 0

substituteTrigraphs' :: Int -> T.Text -> T.Text
substituteTrigraphs' state text =
  case T.uncons text of
    Nothing -> text
    Just (x, xs) ->
      -- we found two ?? in a row, time to check for a trigraph
      if state == 2 then
        case x of
          '='  -> T.cons '#' $ substituteTrigraphs' 0 xs
          '('  -> T.cons '[' $ substituteTrigraphs' 0 xs
          '/'  -> T.cons '\\' $ substituteTrigraphs' 0 xs
          ')'  -> T.cons ']' $ substituteTrigraphs' 0 xs
          '\'' -> T.cons '^' $ substituteTrigraphs' 0 xs
          '<'  -> T.cons '{' $ substituteTrigraphs' 0 xs
          '!'  -> T.cons '|' $ substituteTrigraphs' 0 xs
          '>'  -> T.cons '}' $ substituteTrigraphs' 0 xs
          '-'  -> T.cons '~' $ substituteTrigraphs' 0 xs
          -- in case we got ??? this isn't a trigraph, but there could be one after the first ?
          -- so we just add the ? and restart substituteTrigraph from there
          _   -> T.cons '?' $ substituteTrigraphs' 0 (T.cons '?' $ T.cons x xs)
      else if x == '?'
        then substituteTrigraphs' (state+1) xs
        else if state == 1
          -- false alarm, put ? back
          then T.cons '?' (T.cons x $ substituteTrigraphs' 0 xs)
          -- last case, just a normal character, recurse
          else T.cons x $ substituteTrigraphs' 0 xs

-- 5.1.1.2.2
-- TODO: text source that ends with \
removeSlashedNewline :: T.Text -> T.Text
removeSlashedNewline = T.replace (T.pack "\\\n") (T.pack " ")
