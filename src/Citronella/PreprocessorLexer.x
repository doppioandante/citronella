{
module Citronella.PreprocessorLexer (PreToken(..), PreTokenClass(..), AlexPosn(..), scanner)
where

import qualified Data.Text as T
}

%wrapper "monad"

$digit = 0-9
$nondigit = [_a-zA-Z]
$octaldigit = [0-7]
$hexdigit = [0-9a-fA-F]
$sign = [\-\+]

@identifiernondigit = ($nondigit|\\u $hexdigit{4}|\\U $hexdigit{8})

@escapesequence = \\(['\"\?\\abfnrtv] | $octaldigit{1,3} | x $hexdigit+) 
@cchar = (@escapesequence | $printable # ['\\] )
@schar = (@escapesequence | $printable # [\"\\] )

-- note that the order is important because things like "++" must be matched before "+" (see 6.4p4)
@punctuator = "[" |  "]" |  "(" |  ")" |  "{" |  "}" |  "." |  "->" |  "++" |  "--" |  "&" |  "*" |  "+" |  "-" |  "~" |  "!" |  "/" |  "%" |  "<<" |  ">>" |  "<" |  ">" |  "<=" |  ">=" |  "==" |  "!=" |  "^" |  "|" |  "&&" |  "||" |  "?" |  ":" |  ";" |  "..." |  "=" |  "*=" |  "/=" |  "%=" |  "+=" |  "-=" |  "<<=" |  ">>=" |  "&=" |  "^=" |  "|=" |  "," |  "#" |  "##" |  "<:" |  ":>" |  "<%" |  "%>" |  "%:" |  "%:%:" 

tokens :-
<0>  @identifiernondigit(@identifiernondigit|$digit)*                 { lexeme Identifier }
<0>  ($digit | \.)(@identifiernondigit | [eEpP] $sign | [$digit \.])+ { lexeme PPNumber }
<0>  [uUL]? ' @cchar+ '                                               { lexeme CharacterConstant }
<0>  (u8? | [UL])? \" @schar+ \"                                      { lexeme StringLiteral }
<0>  "//"                                                             { skip `andBegin` state_lineComment}
<state_lineComment> \n                                                { putSpace >> lexeme SourceCharacter `andBegin` 0 }
<state_lineComment> .                                                 ;
<0>  "/*"                                                             { skip `andBegin` state_blockComment}
<state_blockComment> "*/"                                             { putSpace `andBegin` 0 } -- TODO: why isn't skip necessary?
<state_blockComment> .                                                ;
<0>  @punctuator                                                      { lexeme Punctuator }
-- [.] doesn't include the newline character :(
<0>  [.\n]                                                            { lexeme SourceCharacter }

{
-- Preprocessor tokens, 6.4
data PreTokenClass = HeaderName        |
                     Identifier        |
                     PPNumber          |
                     CharacterConstant |
                     StringLiteral     |
                     Punctuator        |
                     SourceCharacter   |
                     EOF
  deriving (Eq, Show)

data PreToken = PreToken AlexPosn PreTokenClass T.Text
  deriving (Eq, Show)

lexeme :: PreTokenClass -> AlexInput -> Int -> Alex PreToken
lexeme tokenClass (pos, _, _, str) len =
  return $ PreToken pos tokenClass text 
  where
    text = case tokenClass of
             Punctuator -> (T.pack $ translateDigraph $ take len str)
             _          -> (T.pack $ take len str)

putSpace :: AlexInput -> Int -> Alex PreToken
putSpace input len = do
  pos <- alexGetPosn -- TODO: is this the right pos information?
  return $ PreToken pos SourceCharacter (T.pack " ")

-- TODO: could be handled separately at lexer level: faster? maybe cleaner
translateDigraph s
  | s == "<:"   = "["
  | s == ":>"   = "]"
  | s == "<%"   = "{"
  | s == "%>"   = "}"
  | s == "%:"   = "#"
  | s == "%:%:" = "##"
  | otherwise   = s

alexGetPosn :: Alex AlexPosn
alexGetPosn = Alex $ \s@AlexState{alex_pos=pos} -> Right (s, pos)

alexEOF :: Alex PreToken
alexEOF = do
    pos <- alexGetPosn
    return $ PreToken pos EOF (T.pack "")

scanner :: T.Text -> Either String [PreToken]
scanner text = runAlex (T.unpack text) scanLoop
  where
    scanLoop = do
        tok <- alexMonadScan
        
        let (PreToken _ tokenClass _) = tok
        if tokenClass == EOF
          then return []
          else do
            toks <- scanLoop
            return $ tok : toks
}
  